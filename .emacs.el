;; Set up package.el to work with MELPA
(require 'package)
(add-to-list 'package-archives
						              '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(package-refresh-contents)

;; Download pacakges
(unless (package-installed-p 'evil)
  (package-install 'evil))

(unless (package-installed-p 'undo-tree)
  (package-install 'undo-tree))

(unless (package-installed-p 'treemacs-evil)
	  (package-install 'treemacs-evil))

(unless (package-installed-p 'doom-themes)
    (package-refresh-contents)
      (package-install 'doom-themes))

(unless (package-installed-p 'use-package)
    (package-refresh-contents)
      (package-install 'use-package))

(unless (package-installed-p 'company)
    (package-refresh-contents)
      (package-install 'company))

(unless (package-installed-p 'company-auctex)
    (package-refresh-contents)
      (package-install 'company-auctex))

(unless (package-installed-p 'all-the-icons)
    (package-refresh-contents)
      (package-install 'all-the-icons))

(unless (package-installed-p 'dashboard)
    (package-refresh-contents)
      (package-install 'dashboard))

(unless (package-installed-p 'emmet-mode)
    (package-refresh-contents)
      (package-install 'emmet-mode))

(unless (package-installed-p 'auctex)
    (package-refresh-contents)
      (package-install 'auctex))

(unless (package-installed-p 'company-irony)
    (package-refresh-contents)
    (package-install 'company-irony))

(unless (package-installed-p 'company-emacs-eclim)
    (package-refresh-contents)
      (package-install 'company-emacs-eclim))

(unless (package-installed-p 'flyspell)
    (package-refresh-contents)
      (package-install 'flyspell))

(unless (package-installed-p 'rustic)
    (package-refresh-contents)
      (package-install 'rustic))

(unless (package-installed-p 'projectile)
    (package-refresh-contents)
      (package-install 'projectile))


;; Enable packages
(require 'evil)
(require 'treemacs-evil)
(require 'doom-themes)
(require 'use-package)
(require 'company)
(require 'company-auctex)
(require 'company-irony)
(require 'dashboard)
(require 'rustic)
(require 'rust-mode)
(require 'emmet-mode)
(require 'undo-tree)

(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)))

(projectile-mode +1)

(add-hook 'sgml-mode-hook 'emmet-mode)   ; Enable Emmet in HTML files
(add-hook 'css-mode-hook 'emmet-mode)    ; Enable Emmet in CSS files
(setq emmet-expand-jsx-className? t)     ; Enable Emmet in JSX files

(define-key emmet-mode-keymap (kbd ",,") 'emmet-expand-line)

(dashboard-setup-startup-hook)

(setq make-backup-files nil)

(add-hook 'after-init-hook 'global-company-mode)

(add-to-list 'company-backends 'company-jedi)
(add-to-list 'company-backends 'company-irony)

(company-emacs-eclim-setup)

(company-auctex-init)

(define-key company-active-map (kbd "TAB") 'company-complete)
(eval-after-load 'company-emacs-eclim
  '(define-key eclim-mode-map (kbd "TAB") 'company-emacs-eclim-complete))
(eval-after-load 'company
  '(define-key company-active-map (kbd "TAB") 'company-complete-common))
;; Set the default font family and size
(set-face-attribute 'default nil
  :family "Luxi Mono"
  :height 140)

;;Set initial buffer to the dashboard
(setq initial-buffer-choice 'emacs-dashboard)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-dracula t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;;Remove annoying bars
(menu-bar-mode -1)
(tool-bar-mode -1)


;;Use numbered lines
(global-display-line-numbers-mode)

(evil-mode 1)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(lsp-mode evil))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;Setup spelling (Requires aspell to be installed)
(setq-default ispell-program-name "aspell")
(setq ispell-dictionary "en_GB")
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)

;;Disable scroll bar
(scroll-bar-mode -1)

;;Setup spellcheck 
(require 'flyspell)
(add-hook 'text-mode-hook #'flyspell-mode)
(add-hook 'prog-mode-hook #'flyspell-prog-mode)
;;Setup ranger
(setq ranger-cleanup-on-disable t)


;;Close bracket pairs
(electric-pair-mode 1)

;;Enable code highlighting
(global-font-lock-mode 1)
;;Setup bindings
(evil-define-command my/ranger ()
  :repeat change
  (interactive)
  (ranger))

(evil-ex-define-cmd "ranger" 'my/ranger)

(evil-define-command my/tree()
  :repeat change
  (interactive)
  (treemacs))

(evil-ex-define-cmd "tree" 'my/tree)

(evil-define-command my/v()
  :repeat change
  (interactive)
  (split-window-right))

(evil-ex-define-cmd "v" 'my/v)
